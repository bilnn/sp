<?php
require __DIR__."/AopSdk.php";
require(dirname(__FILE__)."/../../config.php");
define("SP_PATH",dirname(__FILE__)."/../../SpeedPHP");
define("APP_PATH",dirname(__FILE__)."/../../");
require(SP_PATH."/SpeedPHP.php");


$ticket = addslashes($_POST['ticket']);
$userid = intval($_POST['userid']);
$money = addslashes($_POST['money']);
$taskid = addslashes($_POST['taskid']);
$sign = addslashes($_POST['sign']);

if (empty($ticket) || empty($userid) || empty($money)){
	$action["result"]="fail";
	$action["message"]="参数丢失";
	$action["errcode"]="014";
	$response["action"]=$action;
	exit(json_encode($response));
}
if ($sign != md5($ticket.$userid.$money.sign) && debug==false){
	$action["result"]="fail";
	$action["message"]="签名验证失败";
	$action["errcode"]="-1";
	$response["action"]=$action;
	exit(json_encode($response));
}
$M = spClass("lib_db");
$M->db("jyb_users");
$users = $M->getOne("id,username","id=".$userid." and state=0 and password='".$ticket."'");
if (empty($users)){
	$action["result"]="fail";
	$action["message"]="票据失效，请重新登录";
	$action["errcode"]="-2";
	$response["action"]=$action;
	exit(json_encode($response));
}
$order = date('YmdHis').rand(1111,9999);
$money = $_POST['money'];
$username = $users['username'];

$aop = new AopClient;
$aop->gatewayUrl = "https://openapi.alipay.com/gateway.do";
$aop->appId = APP_ID;
$aop->rsaPrivateKey = APP_PRIVATE_KEY;
$aop->format = "json";
$aop->charset = CHARSET;
$aop->signType = "RSA";
$aop->alipayrsaPublicKey = ALIPAY_PUBLIC_KEY;
//实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
$request = new AlipayTradeAppPayRequest();
//SDK已经封装掉了公共参数，这里只需要传入业务参数

$bizcontent = "{\"body\":\"金元宝用户".$username."充值\","
                . "\"subject\": \"悬赏余额充值\","
                . "\"out_trade_no\": \"".$order."\","
                . "\"timeout_express\": \"30m\","
                . "\"total_amount\": \"".$money."\","
                . "\"product_code\":\"QUICK_MSECURITY_PAY\""
                . "}";
if (!empty($taskid)){
	if ($taskid=="openagent"){
		unset($taskid);
		$bizcontent = "{\"body\":\"开通代理商".$username."支付\","
                . "\"subject\": \"开通代理商支付\","
                . "\"out_trade_no\": \"".$order."\","
                . "\"timeout_express\": \"30m\","
                . "\"total_amount\": \"".$money."\","
                . "\"product_code\":\"QUICK_MSECURITY_PAY\""
                . "}";
	}else if($taskid=="updaagent"){
		unset($taskid);
		$bizcontent = "{\"body\":\"升级代理商".$username."支付\","
                . "\"subject\": \"升级代理商支付\","
                . "\"out_trade_no\": \"".$order."\","
                . "\"timeout_express\": \"30m\","
                . "\"total_amount\": \"".$money."\","
                . "\"product_code\":\"QUICK_MSECURITY_PAY\""
                . "}";

	}else if($taskid=="uppay"){
		unset($taskid);
		$bizcontent = "{\"body\":\"修改任务".$username."支付\","
                . "\"subject\": \"修改任务支付\","
                . "\"out_trade_no\": \"".$order."\","
                . "\"timeout_express\": \"30m\","
                . "\"total_amount\": \"".$money."\","
                . "\"product_code\":\"QUICK_MSECURITY_PAY\""
                . "}";

	}else{
		$bizcontent = "{\"body\":\"悬赏任务".$username."支付\","
                . "\"subject\": \"悬赏任务支付\","
                . "\"out_trade_no\": \"".$order."\","
                . "\"timeout_express\": \"30m\","
                . "\"total_amount\": \"".$money."\","
                . "\"product_code\":\"QUICK_MSECURITY_PAY\""
                . "}";
	}
	
}
$request->setNotifyUrl("http://".$_SERVER['SERVER_NAME']."/api/alipay/notify.php");
$request->setBizContent($bizcontent);
$response = $aop->sdkExecute($request);
//写入订单
$M->db("jyb_order");
$update['order'] = $order;
$update['userid'] = $userid;
$update['times'] = time();
$update['type'] = "alipay";
$update['money'] = $money;
if (!empty($taskid)){
	$update['task_id'] = $taskid;
}

$M->add($update);
echo $response;
?>