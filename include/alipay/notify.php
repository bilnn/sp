<?php
require __DIR__."/AopSdk.php";
require(dirname(__FILE__)."/../../config.php");
define("SP_PATH",dirname(__FILE__)."/../../SpeedPHP");
define("APP_PATH",dirname(__FILE__)."/../../");
require(SP_PATH."/SpeedPHP.php");

$_POST['fund_bill_list'] = str_replace('\"', '"', $_POST['fund_bill_list']);
$aop = new AopClient;
$aop->alipayrsaPublicKey = ALIPAY_PUBLIC_KEY;
$flag = $aop->rsaCheckV1($_POST, NULL, "RSA");
if ($flag){
	$out_trade_no = $_POST['out_trade_no'];
	$trade_no = $_POST['trade_no'];
	$trade_status = $_POST['trade_status'];
	$subject = $_POST['subject'];
	$total_amount = (float)$_POST['total_amount'];
	if ($trade_status=="TRADE_SUCCESS" || $trade_status=="TRADE_FINISHED"){
		$M = spClass("lib_db");
		$M->db("jyb_order");
		$order = $M->getOne("id,money,userid,task_id","`order`='".$out_trade_no."' and state=0");
		if (!empty($order)){
			if ($subject=="悬赏任务支付"){
				$taskid = $order['task_id'];
				$update['trade_no'] = $trade_no;
				$update['state'] = 1;
				$M->edit($update,"`order`='".$out_trade_no."'");
				$money = $order['money'];
				//写入流水
				unset($update);
				$M->db("jyb_user_paylog");
				$update['userid'] = $order['userid'];
				$update['money'] = $money;
				$update['mark'] = "悬赏任务支付".$money."元";
				$update['times'] = time();
				$update['type'] = 1;
				$update['orderid'] = $out_trade_no;
				$update['title'] = "发布任务支出";
				$update['mold'] = 1;
				$update['taskid'] = $taskid;
				$M->add($update);
				unset($update);
				$M->db("jyb_task");
				$task = $M->getOne("title,task_money","status=0 and id=".$taskid);
				$task_money = (float)$task['task_money'];
				if (!empty($task) && $task_money==$total_amount){
					$update['status'] = 1;
					$M->edit($update,"userid=".$order['userid']." and id=".$taskid);
				}
			}else if ($subject=="开通代理商支付" || $subject=="升级代理商支付" || $subject=="修改任务支付"){
				$update['trade_no'] = $trade_no;
				$update['state'] = 1;
				$M->edit($update,"`order`='".$out_trade_no."'");
				$money = $order['money'];
				$M->db("jyb_config");
				$config = $M->getOne("user_cz","id=1");
				$money = $money - $money*$config['user_cz'];
				$M->db("jyb_users");
				$user = $M->getOne("id,money","id=".$order['userid']);
				unset($update);
				$update['money'] = $user['money'] + $money;
				$M->edit($update,"id=".$user['id']);
				//写入流水
				unset($update);
				$M->db("jyb_user_paylog");
				$update['userid'] = $user['id'];
				$update['money'] = $money;
				$update['mark'] = "在线充值".$money."元";
				$update['times'] = time();
				$update['type'] = 0;
				$update['orderid'] = $out_trade_no;
				$update['title'] = "在线充值";
				$update['mold'] = 1;
				$M->add($update);
				if ($subject=="开通代理商支付"){
					$action = "openagent";
					file_get_contents("http://".$_SERVER['SERVER_NAME']."/?c=agent&a=".$action."&userid=".$user['id']);
				}
				if ($subject=="升级代理商支付"){
					$action = "updaagentok";
					file_get_contents("http://".$_SERVER['SERVER_NAME']."/?c=agent&a=".$action."&userid=".$user['id']);
				}
				if ($subject=="修改任务支付"){
					$action = "uppayok";
					file_get_contents("http://".$_SERVER['SERVER_NAME']."/?c=task&a=".$action."&userid=".$user['id']);
				}
				

			}else{
				$update['trade_no'] = $trade_no;
				$update['state'] = 1;
				$M->edit($update,"`order`='".$out_trade_no."'");
				$money = $order['money'];
				$M->db("jyb_config");
				$config = $M->getOne("user_cz","id=1");
				$money = $money - $money*$config['user_cz'];
				$M->db("jyb_users");
				$user = $M->getOne("id,money","id=".$order['userid']);
				unset($update);
				$update['money'] = $user['money'] + $money;
				$M->edit($update,"id=".$user['id']);
				//写入流水
				unset($update);
				$M->db("jyb_user_paylog");
				$update['userid'] = $user['id'];
				$update['money'] = $money;
				$update['mark'] = "在线充值".$money."元";
				$update['times'] = time();
				$update['type'] = 0;
				$update['orderid'] = $out_trade_no;
				$update['title'] = "在线充值";
				$update['mold'] = 1;
				$M->add($update);
			}
		}
	}
echo "success";
}else{
echo "err";
}
?>