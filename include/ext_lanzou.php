<?php
/***********************************************
 *		蓝筹云存储
 * 文件: /include/ext_api.php
 * 说明: 自定义第三方扩展类
 * 作者: Myxf
 * 更新: 2019年5月30日
 ***********************************************/

/**
 * 自定义第三方扩展类
 */
class ext_lanzou {
	private  $username = ""; //蓝筹云的账号
	private  $password = ""; //蓝筹云的密码
	private  $token = null;

	//初始化，如果token为空就获取token
	function __construct(){
		if (empty($this->token)) $this->getToken();
	}

	//上传文件
	public function upfile($path){
		$back = array();
		$_path = realpath($_SERVER['DOCUMENT_ROOT'].$path);
		if (!file_exists(realpath($_SERVER['DOCUMENT_ROOT'].$path))){
			$back['code'] = 1;
			$back['msg'] = "文件不存在或路径错误";
		}else{
			$file_name = basename($_path);
			//由于蓝筹云不允许上传图片，所以改扩展名上传
			$ExtArray = array('jpg','png','gif','bmp');
			$Ext = $this->getExt($_path);

			if (in_array($Ext,$ExtArray)){
				$new_path = str_replace(".".$Ext, ".img", $_path);
				rename($_path,$new_path);
				$_path = $new_path;
			}

			$postdata = array();
			$postdata['task'] = "1";
			$postdata['folder_id'] = "-1";
			$postdata['id'] = "WU_FILE_0";
			$postdata['name'] = basename($_path);
			$postdata['type'] = "application/octet-stream";
			$postdata['upload_file'] = "@". $_path;
			$backdata = $this->vpost("https://up.woozooo.com/fileup.php",$postdata,array('Cookie: '.$this->token));
			$backdata = json_decode($backdata,true);
			if (in_array($Ext,$ExtArray)){
				//上传完成，将本地文件扩展名修改回来
				rename($new_path,str_replace(".img",".".$Ext, $_path));
			}
			if ($backdata['zt']==1){
				$file = array();
				$file['id'] = $backdata['text'][0]['id'];
				$file['f_id'] = $backdata['text'][0]['f_id'];
				$file['name'] = $file_name;
				$back['code'] = 0;
				$back['msg'] = "文件上传成功";
				$back['f_id'] = serialize($file);
				$back['path'] = $path;
				//先测试下载一次,防止180天没下载自动进入回收站
				$this->download($back['f_id']);
			}else{
				$back['code'] = 1;
				$back['msg'] = "文件上传失败";
			}

		}
		return json_encode($back);  
	}

	//获取文件下载地址
	public function download($f_id){
		$f_id = unserialize($f_id);
		$backdata = $this->vget("https://www.lanzous.com/".$f_id['f_id']);
		$down_ifr = "https://www.lanzous.com".$this->strCut($backdata,'" src="','" frameborder');
		$down_url = $this->vget($down_ifr);
		$_t = $this->strCut($down_url,"= '","';",3);
		$_k = $this->strCut($down_url,"= '","';",4);
		$backdata = $this->vpost("https://www.lanzous.com/ajaxm.php","action=down_process&file_id=".$f_id['id']."&t=".$_t."&k=".$_k."&c=",array('referer: https://www.lanzous.com/fn?f='.$f_id['id'].'&t='.$_t.'&k='.$_k,'cookie: noads=1','content-type: application/x-www-form-urlencoded'));
		$backdata = json_decode($backdata,true);
		if ($backdata['zt']==1){
			$path = $backdata['dom']."/file/".$backdata['url'];
			$path = $this->vget($path,array('Accept: */*','Accept-Encoding: gzip, deflate, br','Accept-Language: zh-CN,zh;q=0.9','Cookie: down_ip=1;'),1);
			$path = $this->strCut($path,'Location: ',"\r\n");
			$path = str_replace("&b=", "&bb=", $path)."&q=".$f_id['name'];
			
			$back['code'] = 0;
			$back['msg'] = "下载链接获取成功";
			$back['path'] = $path;
		}else{
			$back['code'] = 1;
			$back['msg'] = "下载链接获取失败";
		}
		return json_encode($back);
	}

	//删除文件，传入f_id
	public function delfile($f_id){
		$f_id = unserialize($f_id);
		$backdata = $this->vpost("https://pc.woozooo.com/doupload.php","task=6&file_id=".$f_id['id'],array('Cookie: '.$this->token));
		$backdata = json_decode($backdata,true);
		if ($backdata['zt']==1){
			$back['code'] = 0;
			$back['msg'] = "文件删除成功";
		}else{
			$back['code'] = 1;
			$back['msg'] = "文件删除失败";
		}
		return json_encode($back);
	}

	//使用账号密码自动登录获取token
	private function getToken(){
		$backdata = $this->vget("https://up.woozooo.com/account.php?action=login&ref=/mydisk.php");
		$formhash = $this->strCut($backdata,'formhash" value="','" />');
		$postdata = "action=login&task=login&username=".$this->username."&password=".$this->password."&formhash=".$formhash;
		$backdata = $this->vpost("https://pc.woozooo.com/account.php",$postdata,"",1);
		$this->token = "phpdisk".$this->strCut($backdata,'Set-Cookie: phpdisk','; expires=');
	}

	//post网络请求
	private function vpost($url,$data,$header = "",$head = 0){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_HEADER, $head);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $tmpInfo = curl_exec($curl);
        if (curl_errno($curl)) {
           return 'Error';
        }
        curl_close($curl);
        return $tmpInfo;
    }

    //get网络请求
    private function vget($url,$header = "",$head = 0){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_HEADER, $head);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $tmpInfo = curl_exec($curl);
        if (curl_errno($curl)) {
           return 'Error';
        }
        curl_close($curl);
        return $tmpInfo;
    }

    //字符串截取
	private function strCut($string,$strstart,$strend,$num = 1){
        $v_a=explode($strstart, $string);
        $v_a=explode($strend, $v_a[$num]);
        return $v_a[0];
    }

    //获取文件扩展名
    private function getExt($file){
		return end(explode(".",$file));
	}
}
?>