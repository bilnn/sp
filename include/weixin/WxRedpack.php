<?php
/**
 * 
 * 微信红包类
 * @author widyhu
 *
 */
class WxRedpack
{
	function send($openid,$money){
		include_once('WxHongBaoHelper.php');
		$wxconfig = new WxPayConfig();
		$commonUtil = new CommonUtil();
        $wxHongBaoHelper = new WxHongBaoHelper();
        $wxHongBaoHelper->setParameter("nonce_str", $this->great_rand());//随机字符串，丌长于 32 位
        $wxHongBaoHelper->setParameter("mch_billno", $wxconfig::MCHID.date('YmdHis').rand(1000, 9999));//订单号
        $wxHongBaoHelper->setParameter("mch_id", $wxconfig::MCHID);//商户号
        $wxHongBaoHelper->setParameter("wxappid", $wxconfig::APPID);
        $wxHongBaoHelper->setParameter("send_name", '江记老豆腐坊');//红包发送者名称
        $wxHongBaoHelper->setParameter("re_openid", $openid);
        $wxHongBaoHelper->setParameter("total_amount", $money*100);//付款金额，单位分
        $wxHongBaoHelper->setParameter("total_num", 1);//红包収放总人数
        $wxHongBaoHelper->setParameter("wishing", '您有一个朋友消费成功，这是您的奖励！');//红包祝福诧
        $wxHongBaoHelper->setParameter("client_ip", '127.0.0.1');//调用接口的机器 Ip 地址
        $wxHongBaoHelper->setParameter("act_name", '推荐好友得红包');//活劢名称
        $wxHongBaoHelper->setParameter("remark", '快打开看看！');//备注信息
        $postXml = $wxHongBaoHelper->create_hongbao_xml();
        $url = 'https://api.mch.weixin.qq.com/mmpaymkttransfers/sendredpack';
        $responseXml = $wxHongBaoHelper->curl_post_ssl($url, $postXml);
		$responseObj = simplexml_load_string($responseXml, 'SimpleXMLElement', LIBXML_NOCDATA);
		return $responseObj;

	}

	public function great_rand(){
        $str = '1234567890abcdefghijklmnopqrstuvwxyz';
		$t1 = "";
        for($i=0;$i<30;$i++){
            $j=rand(0,35);
            $t1 .= $str[$j];
        }
        return $t1;    
    }
}