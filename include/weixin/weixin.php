<?php 
require_once "WxPay.Config.php";
$wxconfig = new WxPayConfig();
$appid = $wxconfig::APPID;
$secret = $wxconfig::APPSECRET;
$act = $_GET["act"];
if ($act=="test"){
  QR_LIMIT_SCENE("44444");
}
if ($act=="test1"){
  echo (time()-filemtime("token.txt"));
  exit();
}
if ($act=="share"){
$token = gettoken($appid,$secret,0);
$time = time();
$wxstr = "bilnn";
$signature = "";
$fxtoken = getfxtoken($token);
//判断token是否有效,无效就强制更新
if (empty($fxtoken)){
$token = gettoken($appid,$secret,1);
$fxtoken = getfxtoken($token);
}
$signature = sha1("jsapi_ticket=" . $fxtoken. "&noncestr=".$wxstr."&timestamp=" .$time. "&url=".urldecode($_GET['url']));
?>
document.write("<script language='javascript' src='https://res.wx.qq.com/open/js/jweixin-1.0.0.js'></script>");
function wxint(wxconfig){
   wx.config({
    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
    appId: "<?php echo $appid;?>", // 必填，公众号的唯一标识
    timestamp: <?php echo $time;?>, // 必填，生成签名的时间戳
    nonceStr: '<?php echo $wxstr;?>', // 必填，生成签名的随机串
    signature: '<?php echo $signature;?>',// 必填，签名，见附录1
    jsApiList: ["checkJsApi", "onMenuShareTimeline", "onMenuShareAppMessage", "onMenuShareQQ", "onMenuShareWeibo", "chooseImage", "previewImage", "uploadImage", "downloadImage", "startRecord", "stopRecord", "onVoiceRecordEnd", "playVoice", "pauseVoice", "stopVoice", "onVoicePlayEnd", "uploadVoice", "downloadVoice"]
   });
   wx.ready(function(){
      wx.onMenuShareTimeline({
        title: wxconfig.shareTitle, // 分享标题
        link: wxconfig.shareUrl, // 分享链接
        imgUrl: wxconfig.imgUrl, // 分享图标
        success: function () { 
           // 用户确认分享后执行的回调函数
           $.ajax({url:"/?c=api&a=share",async:false});
           $(".bargain").hide();
           $(".mybargain").hide();
        },
        cancel: function () { 
          // 用户取消分享后执行的回调函数
        }
      });

      wx.onMenuShareAppMessage({
        title: wxconfig.shareTitle, // 分享标题
        desc: wxconfig.shareTxt, // 分享描述
        link: wxconfig.shareUrl, // 分享链接
        imgUrl: wxconfig.imgUrl, // 分享图标
        type: '', // 分享类型,music、video或link，不填默认为link
        dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
        success: function () { 
          // 用户确认分享后执行的回调函数
        },
        cancel: function () { 
          // 用户取消分享后执行的回调函数
        }
      });

      wx.onMenuShareQQ({
        title: wxconfig.shareTitle, // 分享标题
        desc: wxconfig.shareTxt, // 分享描述
        link: wxconfig.shareUrl, // 分享链接
        imgUrl: wxconfig.imgUrl, // 分享图标
       success: function () { 
         // 用户确认分享后执行的回调函数
       },
       cancel: function () { 
         // 用户取消分享后执行的回调函数
       }
      });

     wx.onMenuShareWeibo({
        title: wxconfig.shareTitle, // 分享标题
        desc: wxconfig.shareTxt, // 分享描述
        link: wxconfig.shareUrl, // 分享链接
        imgUrl: wxconfig.imgUrl, // 分享图标
       success: function () { 
        // 用户确认分享后执行的回调函数
      },
      cancel: function () { 
        // 用户取消分享后执行的回调函数
      }
     });
     wx.error(function(res){
       alert(res.errMsg);
     });
     wx.checkJsApi({
      jsApiList: [
        'getNetworkType',
        'previewImage'
      ],
      success: function (res) {
        //alert(JSON.stringify(res));
      }
     });

   });
  
}
<?php 
}
  //获取token
  function gettoken($appid,$secret,$re=0){
    $wxconfig = new WxPayConfig();
    $path=$_SERVER['DOCUMENT_ROOT']."/api/";
    if (filemtime("token.txt")==false || (time()-filemtime("token.txt"))>6780 || file_get_contents($path."token.txt")=="" || $re==1){
        $token = $wxconfig->vget("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$appid."&secret=".$secret);
        $token = json_decode($token,true);
        $token = $token['access_token'];
        $fp = fopen($path."token.txt", "w");
        fwrite($fp, $token);
        fclose($fp);
    }
    else{
        $token = file_get_contents("token.txt");
    }
    return $token;
  }

  function getfxtoken($token){
    $wxconfig = new WxPayConfig();
    $path=$_SERVER['DOCUMENT_ROOT']."/api/";
    if (filemtime("fxtoken.txt")==false || (time()-filemtime("fxtoken.txt"))>6780 || file_get_contents($path."fxtoken.txt")=="" || $re==1){
        $token = $wxconfig->vget("https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=".$token."&type=jsapi");
        $token = json_decode($token,true);
        $token = $token['ticket'];
        $fp = fopen($path."fxtoken.txt", "w");
        fwrite($fp, $token);
        fclose($fp);
    }
    else{
        $token = file_get_contents("fxtoken.txt");
    }
    return $token;
  }
  //跳转到微信登录
  function wxlogin($back){
    $wxconfig = new WxPayConfig();
    header("location:https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$wxconfig::APPID."&redirect_uri=".urlencode($back)."&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect");
    exit();
  }
  //微信登录返回
  function wxback($code){
      $wxconfig = new WxPayConfig();
        //获取微信授权
      if(!empty($code)){
        $back = $wxconfig->vget("https://api.weixin.qq.com/sns/oauth2/access_token?appid=".$wxconfig::APPID."&secret=".$wxconfig::APPSECRET."&code=".$code."&grant_type=authorization_code");
        $back = json_decode($back,true);
        $back = $wxconfig->vget("https://api.weixin.qq.com/sns/userinfo?access_token=".$back['access_token']."&openid=".$back['openid']);
        $back = json_decode($back,true);
        if (empty($back['openid'])){
            echo "err";
            exit();
        }
        return $back;  
      }
  }
  //生成永久二维码
  function QR_LIMIT_SCENE($str){
    $wxconfig = new WxPayConfig();
    $token = gettoken($wxconfig::APPID,$wxconfig::APPSECRET,0);
    $url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=".$token;
    $data['action_name'] = "QR_LIMIT_SCENE";
    $data['action_info']['scene']['scene_id'] = $str;
    $data = json_encode($data);
    $back = $wxconfig->vpost($url,$data,"");
    $back = json_decode($back,true);
    if (empty($back['ticket'])){
       $token = gettoken($wxconfig::APPID,$wxconfig::APPSECRET,1);
       $url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=".$token;
       $data['action_name'] = "QR_LIMIT_SCENE";
       $data['action_info']['scene']['scene_id'] = $str;
       $data = json_encode($data);
       $back = $wxconfig->vpost($url,$data,"");
       $back = json_decode($back,true);
    }
    return "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=".$back['ticket'];
  }

  function WxRedpack($openid,$money){
     require_once "WxRedpack.php";
     $redpack = new WxRedpack();
     $back = $redpack->send($openid,$money);
     return $back;
  }

  function wxuserinfo($openid){
    $wxconfig = new WxPayConfig();
    $token = gettoken($wxconfig::APPID,$wxconfig::APPSECRET,0);
    $url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=".$token."&openid=".$openid."&lang=zh_CN";
    $back = $wxconfig->vget($url);
    $back = json_decode($back,true);
    if (empty($back['openid'])){
       $token = gettoken($wxconfig::APPID,$wxconfig::APPSECRET,1);
       $url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=".$token."&openid=".$openid."&lang=zh_CN";
       $back = $wxconfig->vget($url);
       $back = json_decode($back,true);
    }
    return $back;
  }
?>