<?php
class lib_db extends spModel
{
  var $pk = "id"; // 数据表的主键
  var $table = ""; // 数据表的名称
  /**
     * 设置表名称
     * @param var $dname 表名
     */
    public function db($dbname){
       $this->table = $dbname;
    }
    /**
     * 获取全部数据
     * @param var $field 字段
     * @param var $where 条件
     * @param var $sort 排序
     * @param var $limit 读取记录数量
     * @param var $spCache 是否缓存查询结果-1为清除缓存，3600为缓存一小时
     */
    public function getAll($field = "*",$where,$sort,$limit = "",$spCache = 0){
        $where = empty($where)? "1=1" : $where;
        $sort = empty($sort)? "" : "order by ".$sort;
        $limit = empty($limit)? $limit : "limit ".$limit;
        $sql="Select ".$field." from `".$this->table."` where ".$where." ".$sort." ".$limit;
        if ($spCache==-1){
            $this->spCache($spCache)->findsql($sql);
            $result = $this->findsql($sql);
        }else if ($spCache == 0){
            $result = $this->findsql($sql);
        }else{
            $result = $this->spCache($spCache)->findsql($sql);
        }
        return $result;
    }

    /**
     * 执行Sql语句
     * @param var $sql sql语句
     */
    public function sql($sql){
        $result = $this->findsql($sql);
        return $result;
    }

    /**
     * 获取一条数据
     * @param var $field 字段
     * @param var $where 条件
     * @param var $spCache 是否缓存查询结果-1为清除缓存，3600为缓存一小时
     */
    public function getOne($field = "*",$where = "1=1",$spCache = 0){
        $sql="Select ".$field." from `".$this->table."` where ".$where." limit 1";
         if ($spCache==-1){
            $this->spCache($spCache)->findsql($sql);
            $result = $this->findsql($sql);
        }else if ($spCache == 0){
            $result = $this->findsql($sql);
        }else{
            $result = $this->spCache($spCache)->findsql($sql);
        }
        return $result[0];
    }

    /**
     * 获取数据总数
     * @param var $where
     */
    public function getCount($where = "1=1"){
        $sql="Select count(*) as num from `".$this->table."` where ".$where;
        $result = $this->findsql($sql);
        return $result[0]["num"];
    }

    /**
     * 获取数据总数去重
     * @param var $table
     * @param var $where
     */
    public function getCounts($table,$where = "1=1"){
        $sql="Select count(distinct ".$table.") as num from `".$this->table."` where ".$where;
        $result = $this->findsql($sql);
        return $result[0]["num"];
    }

    /**
     * 增加一条数据并返回自增id
     * @param array $data
     */
    public function add($data){
        $result = $this->create($data,$this->table);  // 进行新增操作    
        return $result;
    }

    /**
     * 编辑一条数据并返回修改成功数
     * @param array $data
     * @param var $where
     */
    public function edit($data,$where = "1=1"){
        foreach ($data as $key => $one) {
              $sql = $sql."`".$key."` = '".$one."',";
        }
        $sql = $sql."//";
        $sql =  str_replace(",//", "", $sql);
        $sql="update `".$this->table."` set ".$sql." where ".$where;
        $result = $this->edits($sql);
        return $result;
    }

    /**
     * 删除数据若where为空则删除全部
     * @param var $where
     */
    public function del($where = "1=1"){
        $sql="delete from `".$this->table."` where ".$where;
        $result = $this->findsql($sql);
        return $result;
    }

    /**
     * 获取一条字段数据
     * @param var $field 字段
     * @param var $where 条件
     * @param var $empty 默认值
     */
    public function getData($field,$where = "1=1",$empty = ""){
        $sql="Select ".$field." from `".$this->table."` where ".$where." limit 1";
        $result = $this->findsql($sql);
        if (!empty($empty) && empty($result[0][$field])){
           return $empty;
        }
        return $result[0][$field];
    }

    /**
     * 获取求和数据
     * @param var $where table
     * @param var $where 条件
     */
    public function getSum($table,$where="1=1"){
        $sql="Select sum(".$table.") as num from `".$this->table."` where ".$where;
        $result = $this->findsql($sql);
        $result = empty($result[0]["num"]) ? 0 : $result[0]["num"];
        return $result;
    }

}