<?php
define("APP_PATH",dirname(__FILE__));
define("SP_PATH",dirname(__FILE__).'/SpeedPHP');
date_default_timezone_set('PRC');
header('Content-type: text/html; charset=UTF-8');
$modules = basename($_SERVER["SCRIPT_NAME"],".php")=='index' ? "" : '/modules/'.basename($_SERVER["SCRIPT_NAME"],".php");
define("SP_MODULES", basename($_SERVER["SCRIPT_NAME"],".php")); // 模块目录
define("MYTM_PATH", $modules.'/tpl/default/'); // 模板存放的目录
$spConfig = array(
        "db" => array( // 数据库设置
                'driver' => 'mysqli',   // 驱动类型
                'host' => '127.0.0.1',  // 数据库地址，一般都可以是localhost
                'login' => 'root', // 数据库用户名
                'password' => 'toor', // 数据库密码
                'database' => 'test', // 数据库的库名称
        ),
	'view' => array(
                'enabled' => TRUE, // 开启Smarty
                'config' =>array(
                        'template_dir' => $_SERVER['DOCUMENT_ROOT'].constant('MYTM_PATH'), // 模板存放的目录
                        'compile_dir' => $_SERVER['DOCUMENT_ROOT'].'/tmp', // 编译的临时目录
                        'cache_dir' => $_SERVER['DOCUMENT_ROOT'].'/tmp', // 缓存的临时目录
                        'left_delimiter' => '{',  // smarty左限定符
                        'right_delimiter' => '}', // smarty右限定符
                        'caching' => False, // 开启缓存
                        'cache_lifetime' => 3600, // 页面缓存1小时
                ),
	),
        'launch' => array( // 加入挂靠点，以便开始使用Url_ReWrite的功能
                'router_prefilter' => array( 
                        array('spUrlRewrite', 'setReWrite'),  // 对路由进行挂靠，处理转向地址
                ),
                'function_url' => array(
                        array("spUrlRewrite", "getReWrite"),  // 对spUrl进行挂靠，让spUrl可以进行Url_ReWrite地址的生成
                ),
        ),
	'ext' => array(
	        'spVerifyCode' => array( //验证码扩展
        	        'width' => 60, //验证码宽度
        	        'height' => 20, //验证码高度
        	        'length' => 4, //验证码字符长度
        	        'bgcolor' => '#FFFFFF', //背景色
        	        'noisenum' => 50, //图像噪点数量
        	        'fontsize' => 22, //字体大小
        	        'fontfile' => 'font.ttf', //字体文件
        	        'format' => 'gif', //验证码输出图片格式
                ),
        ),
        'spEmail' => array( //邮件扩展的基本设置
                'debug' => FALSE, //调试模式
                'host_name' => 'qq.com', //邮件主机名
                'smtp_host' => 'smtp.qq.com',        //SMTP服务器
                'smtp_port' => '25',        //SMTP端口
                'auth' => TRUE,        //身份验证
                'from' => '', //发件邮箱
                'user' => '',        //用户名
                'pass' => '',        //密码
                'log_file' => '',        //日志文件
                'time_out' => 30,        //超时时间
        ),
        'url' => array( //path_info方式的URL
                'url_path_info' => FALSE, // 是否使用path_info方式的URL
                'url_path_base' => '/'.SP_MODULES.'.php', // URL的根目录访问地址
        ),
        'spUrlRewrite' => array( //伪静态路由规则
                'suffix' => '',
                'sep' => '/', 
                'map' => array(),
                'args' => array(),
        ),
        'spSms' => array( //短信配置
                'accesskey' => '',
                'secret' => '',
                'sign' => '',
        ),
        'spOss' => array(
                'ossurl' => '', //oss域名
        ),
	//'mode' => 'release', //错误提示
	'dispatcher_error' =>"import(APP_PATH.'/ErrorPages/404.html');exit();",
);
?>