<?php
/////////////////////////////////////////////////////////////////
// SpeedPHP中文PHP框架, Copyright (C) 2008 - 2010 SpeedPHP.com //
/////////////////////////////////////////////////////////////////

define("SPANONYMOUS","SPANONYMOUS"); // 无权限设置的角色名称

class spAcl
{
	public $checker = array('spAclModel','check');
	/**
	 * 构造函数，设置权限检查程序与提示程序
	 */
	public function __construct()
	{	
		$params = spExt("spAcl");
		if( !empty($params["checker"]) )$this->checker = $params["checker"];
	}
	/**
	 * 获取当前会话的用户标识
	 */
	public function get()
	{
		return $_SESSION[$GLOBALS['G_SP']['sp_app_id']."_SpAclSession"];
	}

	/**
	 * 强制控制的检查程序，适用于后台。无权限控制的页面均不能进入
	 */
	public function maxcheck()
	{
		$acl_handle = $this->check();
		if( 1 !== $acl_handle ){
			return FALSE;
		}
		return TRUE;
	}

	/**
	 * 有限的权限控制，适用于前台。仅在权限表声明禁止的页面起作用，其他无声明页面均可进入
	 */
	public function mincheck()
	{
		$acl_handle = $this->check();
		if( 0 === $acl_handle ){
			return FALSE;
		}
		return TRUE;
	}
	
	/**
	 * 使用程序调度器进行检查等处理
	 */
	private function check()
	{
		GLOBAL $__controller, $__action;
		$checker = $this->checker;
		$name = $this->get();

		if( is_array($checker) ){
			return spClass($checker[0])->{$checker[1]}($name, $__controller, $__action);
		}else{
			return call_user_func_array($checker, array($name, $__controller, $__action));
		}
	}

	/**
	 * 设置当前用户，内部使用SESSION记录
	 * 
	 * @param acl_name    用户标识：可以是组名或用户名
	 */
	public function set($acl_name)
	{
		$_SESSION[$GLOBALS['G_SP']['sp_app_id']."_SpAclSession"] = $acl_name;
	}
}

 /**
 * ACL操作类，通过数据表确定用户权限
 * 表结构：
 * CREATE TABLE acl
 * (
 * 	aclid int NOT NULL AUTO_INCREMENT,
 * 	name VARCHAR(200) NOT NULL,
 * 	controller VARCHAR(50) NOT NULL,
 * 	action VARCHAR(50) NOT NULL,
 * 	acl_name VARCHAR(50) NOT NULL,
 * 	PRIMARY KEY (aclid)
 * ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci
 */
class spAclModel extends spModel
{

	public $pk = 'aclid';
	/**
	 * 表名
	 */
	public $table = 'ml_acl';

	/**
	 * 检查对应的权限
	 *
	 * 返回1是通过检查，0是不能通过检查（控制器及动作存在但用户标识没有记录）
	 * 返回-1是无该权限控制（即该控制器及动作不存在于权限表中）
	 * 
	 * @param acl_name    用户标识：可以是组名或是用户名
	 * @param controller    控制器名称
	 * @param action    动作名称
	 */
	public function check($acl_name = SPANONYMOUS, $controller, $action)
	{
		$rows = array('controller' => $controller, 'action' => $action );
		if( $acl = $this->findAll($rows) ){
			foreach($acl as $v){
				if($v["acl_name"] == SPANONYMOUS || $v["acl_name"] == $acl_name)return 1;
			}
			return 0;
		}else{
			return -1;
		}
	}
}