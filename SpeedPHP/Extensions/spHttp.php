<?php
/***********************************************
 *      网络请求类扩展类
 * 文件: /library/spHttp.php
 * 说明: 网络请求类扩展类
 * 作者: Myxf
 * 更新: 2015年5月14日
 ***********************************************/

/**
 * 网络请求类扩展类
 */
class spHttp {

    /**
     * post方法
     * @access public
     * @param string $url 请求地址
     * @param string $data 提交数据
     * @param string $header 附加header
     */
    function vpost($url,$data,$header = array()){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $tmpInfo = curl_exec($curl);
        if (curl_errno($curl)) {
           return 'Error';
        }
        curl_close($curl);
        return $tmpInfo;
    }

    /**
     * get方法
     * @access public
     * @param string $url 请求地址
     * @param string $header 附加header
     */
    function vget($url,$header = array()){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1);
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $tmpInfo = curl_exec($curl);
        if (curl_errno($curl)) {
           return 'Error';
        }
        curl_close($curl);
        return $tmpInfo;
    }
    
    /**
     * 采集远程数据到本地
     * @access public
     * @param string $url 请求地址
     */
    function getDataSave($url,$name="",$path="/upload/") {
        $data = $this->vget($url);
        if (empty($name)){
           $name = basename($url);
        }
        if(!is_dir($path)){
            //如果目录不存在，创建目录
            $spUploadFile = spClass("spUploadFile");
            $spUploadFile->set_dir($path);
        }
        file_put_contents($_SERVER['DOCUMENT_ROOT'].$path.$name,$data);
        $url = $path.$name;
        return $url;
    }


}

/* End of this file */